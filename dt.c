/*
 * the struct `d_time' contains the members
 * time_t utime (unix time),
 * tm *local (datetime object, local)
 * char itime (Y.M.D H:M, iso string)
 * char ymd (yyyymmdd)
 * fn (yyyymmdd.txt)
 */

/*
 * display a date from a unix timestamp
 * mode 1: ym yearmonth  (ex: 201612)
 * mode 2: wn weekn      (ex: 48)
 * mode 3: dn dayn       (ex: 339)
 * mode 4: md monthday   (ex: 1203)
 * mode 5: hm hourminute (ex: 2200)
 * mode 6: Ymd
 */

char *display_date(d_time dtime, int mode)
{
  char *timestring = (char*) malloc(sizeof(char)); //[20];
  char *modes[][2] = {
    { "1", "%Y%m" }, // +4 = month
    { "2", "%W" },
    { "3", "%j"},
    { "4", "%m%d"}, // +2 = day
    { "5", "%H:%M"},
  };
  strftime(timestring, 20, modes[mode-1][1], dtime.local);
  return timestring;
}
