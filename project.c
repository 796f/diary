project *new_project(d_time when)
{
  int dead;
  project *proj;
  proj = (project *) malloc(sizeof(*proj));
  strcpy(proj->sdate, when.ymd);
  proj->start = atoi(when.ymd);
  proj->finish = -1;
  printf("  %s\n", when.itime);
  printf("Starting a new project....\n");
  printf("What's the name of your project?\n[30] ");
  scanf(" %s", &(proj->title));
  printf("Does the project have a deadline?");
  dead = response_maker("yn");
  switch(dead) {
  case 1 :
    do {
    printf("Please enter the deadline.\n[yyyymmdd] ");
    fgets(proj->fdate, 14, stdin);
    for (int i=0;i<14;++i) {
      if (proj->fdate[i] == '\n')
	proj->fdate[i] = '\0';
    };
    proj->finish = atoi(proj->fdate);
    printf("%i\n", proj->finish);
    } while ((proj->finish < proj->start)
	     && (proj->finish != 0)
	     );
    printf("\nThe project was began %s and ends %s\n\n",
	   proj->sdate, proj->fdate);
    break;
  case 2 :
    strcpy(proj->fdate, "0");
    break;
  };
  proj->category[0][0] = 0;
  proj->imp = 0;
  display_project(proj);
  char **buffer = project2csv(proj);
  char **buff_ = buffer;
  printf("%i", sizeof(buffer));
  for (int i=0; i<sizeof(buffer); ++i) {
    printf("\n~%s", *(buff_+i));
  };
  printf("\n%s\n", *(buffer));
  return proj;
}

void display_project(project *proj)
{
  printf("\nTitle: %s", proj->title);
  printf("\nStart date: %s", proj->sdate);
  printf("\nFinish date: %s", proj->fdate);
  if ((proj->finish - proj->start) > 0)
    printf("\nDays remaining: %i", (proj->finish - proj->start));
  printf("\nImportance: %i", proj->imp);
  printf("\n----------\n");
}
