/*
 * simple library for handling CSV
 * that is the backbone of the diary.c
 * project
 */

/* returns an array of files matching a wildcard */
char **indexposts()
{
  char **list = malloc(30 * sizeof(char*));
  for (int i=0; i < 30; ++i)
    list[i] = malloc(20 * sizeof(char));
  int cnt = 0;
  DIR *d;
  struct dirent *dir;
  d = opendir(".");
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_name[0] == '2') {
	list[cnt] = dir->d_name;
	cnt++;
      };
    };
    closedir(d);
  }
    
  return list;
}

/* check to see if a file exists */
int checkfile(char fn[])
{
  if (access(fn, F_OK) != -1) {
    return 1;
  } else {
    return 0;
  };
}

/* return a file as char array */ 
char *loadfile(char fn[])
{
  if (!checkfile(fn))
    return "Error";
  FILE *f = fopen(fn, "rb");
  fseek(f, 0, SEEK_END);
  int fsize = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = (char*)malloc(fsize+1);
  fread(buffer,fsize, 1, f);
  fclose(f);
  buffer[fsize] = 0;
  
  return buffer;
}

char **project2csv(project *proj)
{
  char **buffer = malloc(4*(sizeof(char*)));
  for (int i=0; i<4; ++i)
    buffer[i] = malloc(61 * sizeof(char));
  strcpy(buffer[0], proj->title);
  strcpy(buffer[1], "0");
  strcpy(buffer[2], proj->sdate);
  strcpy(buffer[3], proj->fdate);
  return buffer;
}

void writefile(char fn[], char buffer[])
{
  FILE *f = fopen(fn, "ab");
  if (f != NULL) {
      fputs(buffer, f);
      fclose(f);
  };  
}
