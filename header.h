#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>

/* color codes */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

/* 
 * contains UNIX time string (int),
 * ISO-formatted time (string),
 * and a local time struct
 */

typedef struct {
  time_t utime;
  char itime[20];
  struct tm *local;
  char ymd[12];
  char fn[18];
} d_time;

typedef struct {
  char title[30];
  char category[10];
  char fdate[25];
  int idate;
  char entry[9001];
} a_diary;

typedef struct {
  char title[31];
  char category[12][5];
  char sdate[12]; // start date
  int start;
  char fdate[12]; // finish date
  int finish;
  int imp; // 0-4 importance
} project;

/* csv.c */
char **indexposts();
int checkfile(char fn[]);
char *loadfile(char fn[]);
char **project2csv(project *proj);
void writefile(char fn[], char buffer[]);

/* dt.c */
char *display_date(d_time dtime, int mode);

/* diary.c */
int main();

/* menu.c */
int response_maker(char r_type[]);
void welcome_menu(d_time now);
void travel_menu(d_time now);
void now_menu(d_time now);
void future_menu(d_time now);
void edit_journal(d_time now);

/* project.c */
project *new_project(d_time when);
void display_project(project *proj);
