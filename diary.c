/*
 * Simple diary app for (lu)nix command lines
 * Users can store notes for each day or set
 * appointments / entries for future topics
 * 
 * uses unix time / ISO 8601 time standards,
 * CSV, etc.
 * 
 * public domain (!c) 4x13.net  2016
 */

#include "header.h"
#include "menu.c"
#include "csv.c"
#include "dt.c"
#include "project.c"

int main()
{
  char filename[] = "test.txt";
  d_time now;
  char **file_list = indexposts();
  //  printf(sizeof(file_list));
  int file_len = (sizeof (*file_list) /sizeof (*file_list[0]));
  int i = 0;
  //  while (strlen(*file_list)) {
  //    printf("\n*. %s \n", *file_list);
    //    file_list++;
  //  };
  
  now.utime = time(0);
  now.local = localtime(&now.utime);
  strftime(now.itime, 19, "%Y.%m.%d %H:%M", now.local);
  strftime(now.ymd, 9, "%Y%m%d", now.local);
  snprintf(now.fn, 18, "%s.txt",  now.ymd);  
  if (!checkfile(now.fn)) {
    writefile(now.fn, "test\n");
  };
  
  char *buffer = loadfile(now.fn); // initial file to load
  //  printf("%s", buffer);
  welcome_menu(now);
  travel_menu(now);
  
  if (checkfile(now.fn))
    free(buffer);
  return 0;
} 
