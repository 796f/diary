/*
 * Various menus for handling the diary functions
 * including welcoming, accessing different files,
 * etc. Currently uses a weak list data structure
 * and switches with a nice i/o looper
 */

/* 
 * Prompts a user for input, and returns
 * their choice, if it is in the options, as an
 * int, keeping them in the loop otherwise, unless
 * they want to exit with special entries Q and Z.
 * 
 * example: 
 * response_maker("abc"); prompts the user for
 * [a/b/c] 
 *  ^--    styled input, where input a-> 1, 
 *                            b-> 2, c-> 3, etc
 * case q/z are (-1), exit, 
 * case h is (-2), help
 */

int response_maker(char r_type[])
{
  int r_len = strlen(r_type);
  int p_len = (r_len * 2) + 3;
  int p_cnt = -1;
  int i = 0;
  char choices[r_len];
  char promptbox[p_len];
  char response[3];
  
  promptbox[0] = '[';
  for (i; i < r_len; ++i) {
    p_cnt += 2;
    choices[i] = r_type[i];
    promptbox[p_cnt] = choices[i];
    promptbox[p_cnt+1] = '/';
  };
  promptbox[p_cnt+1] = ']';
  promptbox[p_cnt+2] = 0;
  printf("\n");
  for (i; i<(p_len+1); ++i) {
    printf("_");
  };
  printf("\n%s> ", promptbox);
  fgets(response, 3, stdin);
  response[0] = tolower(response[0]);
  for (i=0; i<r_len;++i) {
    if (response[0] == r_type[i])
      return i+1;
  }
  
  if (0 > response[0])
    exit(0);
  switch(response[0]) {
  case 'q':
    exit(0);
  case 'z':
    exit(0);
  case 'h':
    printf("\nhelp me!\n");
    return -1;
  case 'b':
    printf("\nBack attempted..\n-------\n");
    return -2;
  };
  
  return response_maker(r_type);
}

/* says today's date and welcomes the user  */
void welcome_menu(d_time now)
{
  printf("Note: `q' and `z' exit. `h' can often provide more help.\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  printf("\nToday is %s%s%s\n", KCYN, now.itime, KNRM);
  system("cal");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}  

/* see if a user wants to work in the past, present, or future */
void travel_menu(d_time now)
{
  char response[5];
  printf("go to the [P]ast?\n"
	 "     (<-- p)\n\n                  (.n)\n"
	 "           stay in the [N]ow? \n\n"
	 "                            Or go [F]orward?\n"
	 "                                   (f -->)\n");
  int mode = response_maker("pnf");  
  switch(mode) {
  case -2:
    return;
  case -1 :
    printf(" %sp%sast shows recently expired entries",
	   KYEL, KNRM);
    printf("\n %sn%sow lets you focus on today and the short term",
	   KMAG, KNRM);
    printf("\n %sf%suture is for planning ahead.\n\n",
	   KCYN, KNRM);
    travel_menu(now);
  case 1 :
    printf("@ past\n");
    printf("TO BE ADDED\n");
    break;
  case 2 :
    printf("@ now\n");
    printf("-----\nopen ->%s\n-----\n%s\n", now.fn, loadfile(now.fn));
    now_menu(now);
    break;
  case 3 :
    printf("@ future\n");
    future_menu(now);
    break;
  default :
    printf("\nTO BE ADDED\n");
    printf("%i (%c) mode", mode, mode);
    break;
  };
  travel_menu(now);
}

/* the menu for working in the now */
void now_menu(d_time now)
{
  printf("---------------------\n");
  printf("Would you like to write a new [J]ournal entry,\n"
	 "start a new [P]roject, or add an [A]ppointment?");
  int choice = response_maker("jpa");
  project *new;
  switch(choice) {
  case -2 :
    travel_menu(now);
    break;
  case -1 :
    printf(" j is for adding notes to a record for today");
    printf("\n p can help you structure projects.");
    printf("\n a can store appointments, meetings, or due dates");
    printf("\n b takes you back.\n\n");
    now_menu(now);
    break;
  case 1 :
    edit_journal(now);
    break;
  case 2 :
    new = new_project(now);
    if (new->title)
      printf("\n%s\n", new->fdate);
    break;
  default :
    printf("\n? You picked: #%i\n", choice);
    break;
  };
}

void future_menu(d_time now)
{
  printf("You may do several things:\n"
	 " * Schedule an appointment [a]\n"
	 " * See upcoming due dates [d]\n"
	 " * See 1 and 3 month agendas[1/3]\n");
  int choice = response_maker("ad13");
  project *new;
  switch(choice) {
  case -2 :
    travel_menu(now);
    break;
  case -1 :
    future_menu(now);
    break;
  case 1 :
    printf("FUTURE APPOINTMENT");
    break;
  case 2 :
    printf("PROJECT DUE DATES");
    break;
  case 3 :
    printf("  1 MONTH:\n[continue] ");
    getchar();
    printf("\n");
    if (!checkfile("1month.txt"))
	system("cal > 1month.txt");
    printf(loadfile("1month.txt"));
    break;
  case 4 :
    printf("  3 MONTHS\n[continue] ");
    getchar();
    printf("\n");
    if (!checkfile("1month.txt"))
      system("cal -S -3 > 3month.txt");
    printf(loadfile("3month.txt"));
    break;
  }
  printf("\n\n");
} 
	 

void edit_journal(d_time now)
{
  char command[30];
  printf("%s\n", now.fn);
  printf("Loading file in your editor...\n");
  printf(loadfile(now.fn));
  strcpy(command, "emacs -nw ");
  strcat(command, now.fn);
  system(command);
  now_menu(now);
}

